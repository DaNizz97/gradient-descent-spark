package com.siemens.test.gradient_descent;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.broadcast.Broadcast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GradientDescent implements Serializable {
    private static final double ALPHA = 0.0000001;
    private static final int COUNT_OF_REPEATS = 10000;

    private JavaRDD<FeatureVector> dataset;
    private Broadcast<Long> countOfPoints;
    private Regression targetFunction;
    private double alpha;
    private int countOfIterations;


    public GradientDescent(JavaRDD<FeatureVector> featureVectors, Regression targetFunction, Broadcast<Long> countOfPoints, double alpha, int countOfIterations) {
        this.dataset = featureVectors;
        this.countOfPoints = countOfPoints;
        this.targetFunction = targetFunction;
        this.alpha = alpha;
        this.countOfIterations = countOfIterations;
    }

    public GradientDescent(JavaRDD<FeatureVector> featureVectors, Regression targetFunction, Broadcast<Long> countOfPoints) {
        this.dataset = featureVectors;
        this.countOfPoints = countOfPoints;
        this.targetFunction = targetFunction;
        this.alpha = ALPHA;
        this.countOfIterations = COUNT_OF_REPEATS;
    }

    private LinearRegression train() {
        List<Double> thetaVector = targetFunction.getThetaVector();
        List<Double> newThetaVector = new ArrayList<>(thetaVector.size());

        for (int j = 0; j < thetaVector.size(); j++) {
            double sumErrors = calculateSumErrors(j);
            double gradient = (1.0 / countOfPoints.getValue()) * sumErrors;
            newThetaVector.add(thetaVector.get(j) - alpha * gradient);
        }
        return new LinearRegression(newThetaVector);
    }

    private double calculateSumErrors(int j) {
        return dataset.map(featureVector -> (j == 0) ? targetFunction.apply(featureVector) - featureVector.getCoordinate(0) :
                (targetFunction.apply(featureVector) - featureVector.getCoordinate(0)) * featureVector.getCoordinate(j))
                .reduce((d1, d2) -> d1 + d2);
    }

    public List<Double> learn() {
        for (int i = 0; i < countOfIterations; i++) {
            targetFunction = train();
            setTargetFunction(targetFunction);
        }
        return targetFunction.getThetaVector();
    }

    public void setTargetFunction(Regression targetFunction) {
        this.targetFunction = targetFunction;
    }

    public void setCountOfPoints(Broadcast<Long> countOfPoints) {
        this.countOfPoints = countOfPoints;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }
}
