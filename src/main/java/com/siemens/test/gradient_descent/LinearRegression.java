package com.siemens.test.gradient_descent;

import java.util.List;

public class LinearRegression implements Regression {
    private final List<Double> thetaVector;

    public LinearRegression(List<Double> thetaVector) {
        this.thetaVector = thetaVector;
    }

    public Double apply(FeatureVector featureVector) {
        double prediction = thetaVector.get(0);
        for (int i = 1; i < thetaVector.size(); i++) {
            prediction += thetaVector.get(i) * featureVector.getCoordinate(i);
        }
        return prediction;
    }

    public List<Double> getThetaVector() {
        return thetaVector;
    }
}
