package com.siemens.test.gradient_descent;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class TSVParser implements DataParser {

    public FeatureVector parseLineOfCoordinates(String s) {
        StringTokenizer st = new StringTokenizer(s);
        ArrayList<Double> coordinates = new ArrayList<>();
        while (st.hasMoreTokens()) {
            coordinates.add(Double.parseDouble(st.nextToken()));
        }
        return new FeatureVector(coordinates);
    }
}
