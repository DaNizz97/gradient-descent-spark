package com.siemens.test.gradient_descent;

import java.util.List;

public class QuadraticRegression implements Regression {
    private final List<Double> thetaVector;

    public QuadraticRegression(List<Double> thetaVector) {
        this.thetaVector = thetaVector;
    }

    @Override
    public Double apply(FeatureVector featureVector) {
        if (featureVector.size() != (thetaVector.size() - 1) / 2) {
            throw new IllegalArgumentException();
        }
        double prediction = thetaVector.get(0);
        for (int i = 1; i < featureVector.size(); i++) {
            prediction += thetaVector.get(i * 2 - 1) * featureVector.getCoordinate(i)
                    +
                    thetaVector.get(i * 2) * Math.pow(featureVector.getCoordinate(i), 2);
        }
        return prediction;
    }

    public List<Double> getThetaVector() {
        return thetaVector;
    }
}
