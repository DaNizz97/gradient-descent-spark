package com.siemens.test.gradient_descent;

import java.io.Serializable;
import java.util.List;
import java.util.function.Function;

public interface Regression extends Function<FeatureVector, Double>, Serializable {
    void setThetaVector();
    List<Double> getThetaVector();
}
