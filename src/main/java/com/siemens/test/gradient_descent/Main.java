package com.siemens.test.gradient_descent;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        if (args.length < 2)
            throw new IllegalArgumentException("Not few arguments! You must specify a master's URL and filename with data");

        SparkConf sc = new SparkConf();
        sc.setMaster(args[0]).setAppName("Gradient descent app");
        JavaSparkContext jsc = new JavaSparkContext(sc);

        DataParser dataParser = new TSVParser();

        JavaRDD<FeatureVector> dataset = jsc.textFile(args[1]).map(dataParser::parseLineOfCoordinates).cache();

        double alpha = (args.length > 2) ? Double.parseDouble(args[2]) : 0.00001;

        int countOfIterations = args.length > 3 ? Integer.parseInt(args[3]) : 10000;

        Regression targetFunction = null;

        if (args.length > 4) {
            if (args[5].equals("Q")) {
                targetFunction = new QuadraticRegression(Arrays.asList(0.0, 0.0, 0.0));
            } else if (args[5].equals("L")) {
                targetFunction = new LinearRegression(Arrays.asList(0.0, 0.0));
            }
        } else {
            targetFunction = new LinearRegression(Arrays.asList(0.0, 0.0));
        }

        GradientDescent gradientDescent = new GradientDescent(dataset, targetFunction, jsc.broadcast(dataset.count()), alpha, countOfIterations);

        System.out.println(gradientDescent.learn().toString());

        jsc.stop();
    }
}
