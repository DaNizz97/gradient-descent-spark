package com.siemens.test.gradient_descent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class FeatureVector implements Serializable {
    private ArrayList<Double> coordinates;

    public FeatureVector(int n) {
        this.coordinates = new ArrayList<>(Collections.nCopies(n, 0.0));
    }

    public FeatureVector(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public Double getCoordinate(int index) {
        return this.coordinates.get(index);
    }

    public int size() {
        return coordinates.size();
    }

    public void add(Double coord) {
        this.coordinates.add(coord);
    }
}
