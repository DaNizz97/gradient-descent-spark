package com.siemens.test.gradient_descent;

import java.io.Serializable;

public interface DataParser extends Serializable {
    FeatureVector parseLineOfCoordinates(String s);
}
