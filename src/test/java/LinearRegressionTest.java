import com.siemens.test.gradient_descent.FeatureVector;
import com.siemens.test.gradient_descent.LinearRegression;
import com.siemens.test.gradient_descent.Regression;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class LinearRegressionTest {

    private Regression fun;

    @Before
    public void setUp() {
        fun = new LinearRegression(Arrays.asList(1.0, 1.0));
    }

    @Test
    public void applyTest() {
        FeatureVector featureVector = new FeatureVector(1);
        featureVector.add(10d);

        Double prediction = fun.apply(featureVector);
        Double expected = 11d;
        assertEquals(expected, prediction);
    }
}