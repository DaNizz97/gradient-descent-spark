import com.siemens.test.gradient_descent.FeatureVector;
import com.siemens.test.gradient_descent.GradientDescent;
import com.siemens.test.gradient_descent.LinearRegression;
import com.siemens.test.gradient_descent.Regression;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class GradientDescentTest {

    private JavaSparkContext jsc;
    private Regression targetFunction;

    @org.junit.Before
    public void setUp() {
        SparkConf conf = new SparkConf();
        conf.setAppName("Linear Regression Test").setMaster("local");
        jsc = new JavaSparkContext(conf);
        List<Double> thetas = Arrays.asList(0.1, 1.1);
        targetFunction = new LinearRegression(thetas);
    }

    @Test
    public void gradientTest() {
        List<FeatureVector> featureVectorList = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            ArrayList<Double> point = new ArrayList<>();
            point.add((double) i);
            point.add((double) i);
            featureVectorList.add(new FeatureVector(point));
        }

        GradientDescent gradientDescent =
                new GradientDescent(jsc.parallelize(featureVectorList),
                        targetFunction, jsc.broadcast((long) featureVectorList.size()),
                        0.0001, 100);

        List<Double> nthetas = gradientDescent.learn();
        List<Double> expectedThetas = Arrays.asList(0.0, 1.0);
        double[] a = {expectedThetas.get(0), expectedThetas.get(1)};
        double[] b = {nthetas.get(0), nthetas.get(1)};

        assertArrayEquals(a, b, 0.1);
    }
}